import exercise
import unittest


class TestFile(unittest.TestCase):
    def test_read_file(self):
        password = exercise.validate_password("aaabbb")
        self.assertFalse(password)

        password = exercise.validate_password("aaaABB123")
        self.assertFalse(password)

        password = exercise.validate_password("Abc123@09")
        self.assertTrue(password)

if __name__ == '__main__':
    unittest.main()
