import re

students_record = {}

def students_store(file):
    """
    function to find student ID
    """
    try:
        with open(file, "r") as students:
            for student in students:
                student_id = student.strip().split(",")
                id_stud = student_id[0].strip().split(":")[1]
                students_record.update({id_stud: student})
                print(students_record)
                
    except FileNotFoundError:
        print("File not found!!")

def is_id_valid(student_id):
    """
    function to verify if student ID valid
    """
    if students_record.get(student_id):
        return True
    return False


def validate_password(password):
    """
    function to validate password entered by the user
    """
    special_character = '[@_#$%^&*()<>?/|}{~:]'
    if len(password) < 8:
        print("Make sure your password is at lest 8 letters")
    elif re.search('[0-9A-Za-z]', password) and re.search(special_character, password) is  None:
        print("Password should contain number, uppercase, and lowercase letters and special character")
    else:
        return password

def get_saved_student(student_id):
    if students_record.get(student_id):
        return True
    else:
        return False

def students_updated(file, student_id):
    try:
        with open(file, "r") as final_file:
            for student in final_file:
                if student.startswith(f'id:{student_id}'):
                    return student
    except FileNotFoundError:
        print("File not found")


def update_final_list(file, student_id, saved_student):
    if students_updated(file, student_id):
        return False
    try:
        with open(file, 'a') as final_file:
            final_file.write(saved_student)
    except FileNotFoundError:
        return False
    return True

