import json
import xml.etree.ElementTree as ET
import xml.dom.minidom as minidom

import csv

class JsonSerializer:
    def __init__(self):
        self._current_object = None

    def student_file(self):
        json_data = {}
        with open("final_students.txt", "r") as existing_file:
            content = existing_file.read()
            splitted_file = content.split()

            st_arr = []
            for line in splitted_file:
                split_data = line.split(',')
                for i in split_data:
                    new_arr = i.split(':', 1)
                    json_data[new_arr[0]] = new_arr[1].split(',')[0]
                st_arr.append(json_data)

        file = json.dumps(st_arr, indent=4, sort_keys=True)
        file = file.encode(encoding="UTF-8")

class CsvSerializer:
    def __init__(self):
        self._element = None

    def student_file(self):
        with open("final_students.txt", 'r') as csv_format:
            row_line = csv_format.readlines()
            with open('final_students.csv', 'w+',  newline = '') as csvFile:
                csvWriter = csv.writer(csvFile, delimiter=',')
                csvWriter.writerow(('Id','Fistname','Lastname','Password'))
                print("A file with extension .csv is successfuly created!!")

                for elem in row_line:
                    new_elem = elem.replace(":", ",")
                    remove_elem = new_elem.strip().split(",")
                    remove_elem.remove('id')
                    remove_elem.remove('firstname')
                    remove_elem.remove('lastname')
                    remove_elem.remove('password')
                    csvWriter.writerow(remove_elem)

class XmlSerializer:
    def __init__(self):
        self._element = None

    def student_file(self):
        with open("final_students.txt", 'r') as csv_file:
            row_line = csv_file.readlines()
            root = ET.Element('root')

            for line in row_line:
                student_data = line.replace(":", ",")
                split_data = student_data.split(",")
                student = ET.SubElement(root, split_data[0])
                student.text = student_data

                xml_format = minidom.parseString(ET.tostring(root)).toprettyxml(indent="  ",encoding='utf-8')


class serializerFactory:
    def __init__(self):
        self._creators = {}

    def register_format(self, format, creator):
        self._creators[format] = creator

    def get_serializer(self, format):
        creator = self._creators.get(format)
        if not creator:
            raise ValueError(format)
        return creator()


class ObjectSerializer:
    def serialize(self, student_data, format):
        serializer = factory.get_serializer(format)
        student_data.serialize(serializer)

factory = serializerFactory()
factory.register_format('JSON', JsonSerializer)
factory.register_format('XML', XmlSerializer)
re = factory.register_format('CSV', CsvSerializer)
