import socket

HOST = '127.0.0.1'
PORT = 65431
data = ''

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    s.listen()
    conn, addr = s.accept()


    with conn:
        print("Connected by ", addr)
        while True:
            data = conn.recv(1024).decode(encoding="UTF-8")
            file_data = data.strip().split('-')
            file_format = file_data[0]
            print(file_format)
            if file_data[-1] == 'JSON':
                with open("final_students.json", 'w') as file:
                    file.write(file_format)
            elif file_data[-1] == 'XML':
                with open("final_students.xml", 'w') as file:
                    file.write(data)
            elif file_data[-1] == 'CSV':
                with open("final_students.csv", 'w') as file:
                    file.write(file_format)
            if not data:
                break





