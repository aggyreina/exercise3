import serializers
import exercise2

file_format = input("Enter file format: ")

student_data = exercise2.Student('student')
serializer = serializers.ObjectSerializer()
serializer.serialize(student_data, file_format.upper())
