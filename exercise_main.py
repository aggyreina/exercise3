import exercise as exe

students_file = "students.txt"
final_students_file= "final_students.txt"

exe.students_store(students_file)

student_id = input("enter your student ID: ")

if not exe.is_id_valid(student_id):
    print("Provided student ID not found")
else:
    print("\n")
    print("You are allowed 3 trials")
    print("--------------------------")
    student_password = input("Enter a password: ")
    validate_password = exe.validate_password(student_password)

    trials = 0
    n_trials = 3

    while not validate_password and trials < n_trials - 1:
        another_pass = input("Invalid password, please try again: ")
        validate_password = exe.validate_password(another_pass)
        trials += 1

    if not validate_password:
        print("No trial left, restart the process")


saved_student = exe.students_updated(students_file, student_id)
saved_student = saved_student.replace("\n", "")

saved_student = f'{saved_student},password:{student_password}\n'

if exe.update_final_list(final_students_file, student_id, saved_student):
    print("Student list updated")
else:
    print("Students' record is already updated")

